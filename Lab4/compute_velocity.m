function [value] = compute_velocity(t)

g=9.81;
v=750;
m0=15*1e4;
q=2700;
u=2000;

value=v-u*log(m0/(m0-q*t))-g*t;

end



