function [xvect,xdif,fx,it_cnt]=bisect(a,b,eps,fun)


for i = 1:1000
    % bisection algorithm
    x = (a + b)/2;
    
    xvect(i)=x;
    fx(i)=feval(fun,x);
    
    if i == 1
        xdif(i)=xvect(i);
    else
        xdif(i)= abs(xvect(i-1)-xvect(i));
    end
    
    if abs(fx(i)) < eps || abs(a-b) < eps
        it_cnt=i;
        break;
    elseif feval(fun,a)*fx(i) < 0
        b=x;
    else
        a=x;
    end
    
    % use feval to obtain the value of the function in 'x' 
end


end
