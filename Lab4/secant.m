function [xvect,xdif,fx,it_cnt]=secant(a,b,eps,fun)


for i = 1:1000
    % bisection algorithm
    b_fun=feval(fun,b);
    a_fun=feval(fun,a);
    x = b-(b_fun*(b-a))/(b_fun-a_fun);
    xvect(i)=x;
    fx(i)=feval(fun,x);
    if abs(fx(i)) < eps || abs(a-b) < eps
        it_cnt=i;
        break;
    end
    a = b;
    b = x;
    
    if i == 1
        xdif(i)=xvect(i);
    else
        xdif(i)= abs(xvect(i-1)-xvect(i));
    end
  
    % use feval to obtain the value of the function in 'x' 
end


end

