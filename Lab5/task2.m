clc 
clear all
close all


cap = 45;

DivPoly = zeros(cap,1);
DivTryg = zeros(cap,1);
FFPolyLast = zeros(101,101);
FFTrygLast = zeros(101,101);

for i=5:cap
    [x,y,f]=lazik(i);
    [XX,YY]=meshgrid(linspace(0,100,101),linspace(0,100,101));
    [p]=polyfit2d(x,y,f);
    [FF]=polyval2d(XX,YY,p);
    DivPoly(i)=max(max(FF-FFPolyLast));
    FFPolyLast = FF;
    [p]=trygfit2d(x,y,f);
    [FF]=trygval2d(XX,YY,p);  
    DivTryg(i)=max(max(FF-FFTrygLast));   
    FFTrygLast = FF;
end


figure();
plot(5:cap,DivPoly(5:cap));
title('Polynomial Interpolation Differences');
xlabel('Number of samples');
ylabel('Biggest difference');
saveas (gcf,strcat('./Task2Plots/PolynomialInterpolationDifferences.png'));
figure();
plot(5:cap,DivTryg(5:cap));
title('Trigonometric Interpolation Differences');
xlabel('Number of samples');
ylabel('Biggest difference');
saveas (gcf,strcat('./Task2Plots/TrigonometricInterpolationDifferences.png'));