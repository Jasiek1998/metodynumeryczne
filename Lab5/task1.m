clc
clear all
close all

K=[5,15,25,35];


for i = 1:4
    figure();
    [x,y,f]=lazik(K(i));
    [p]=polyfit2d(x,y,f);
    [XX,YY]=meshgrid(linspace(0,100,101),linspace(0,100,101));
    [FF]=polyval2d(XX,YY,p);
    subplot(2,2,1);
    plot(x,y,'-o','linewidth',3);
    title('Tor ruchu �azika');
    xlabel('x[m]');
    ylabel('y[m]');
    subplot(2,2,2);
    plot3(x,y,f,'o');
    title('Zebrane warto�ci pr�bek');
    xlabel('x[m]');
    ylabel('y[m]');
    zlabel('f(x,y)');
    subplot(2,2,3);
    surf(XX,YY,FF);
    title('Interpolacja wielomianowa');
    xlabel('x[m]');
    ylabel('y[m]');
    zlabel('f(x,y)');
    [p]=trygfit2d(x,y,f);
    [FF]=trygval2d(XX,YY,p);
    subplot(2,2,4);
    surf(XX,YY,FF);
    title('Interpolacja trygonometryczna');
    xlabel('x[m]');
    ylabel('y[m]');
    zlabel('f(x,y)');
    saveas (gcf,strcat('./Task1Plots/K=',int2str(K(i)),'.png'));
end