clc
clear all
close all

% komentowanie/ odkomentowywanie: ctrl+r / ctrl+t

% Zadanie A
% ------------------
N = 10;
density = 3; % parametr decyduj�cy o gestosci polaczen miedzy stronami
[Edges] = generate_network(N, density,172090);
% -----------------

% Zadanie B
% ------------------
% generacja macierzy I, A, B i wektora b
...
d=0.85;
B = sparse(Edges(2,:),Edges(1,:),1);  % macierze A, B i I musz� by� przechowywane w formacie sparse (rzadkim)
A = sparse(diag(1./sum(B)));
I = speye(N);

save zadB_172090 A B I b
% -----------------
%Zadanie C
 M = I-d*B*A;
 b(1:N,1)=(1-d)/N;
 r=(M\b)';
 save zadC_172090 r

%Zadanie D
% ------------------
clc
clear all
close all

N = [500, 1000, 3000, 6000, 12000];


for i = 1:5
tic
density = 10;
[Edges]=generate_network(N(i),density,172090);
d=0.85;
B = sparse(Edges(2,:),Edges(1,:),1);  % macierze A, B i I musz� by� przechowywane w formacie sparse (rzadkim)
A = sparse(diag(1./sum(B)));
I = speye(N(i));
M = I-d*B*A;
b(1:N(i),1)=(1-d)/N(i);
r=(M\b)';
czas_Gauss(i) = toc;
end

plot(N, czas_Gauss);
saveas (gcf,'zadD_172090.png');
%------------------



% Zadanie E
%------------------
clc
clear all
close all
N = [500, 1000, 3000, 6000, 12000];
% przyk�ad dzia�ania funkcji tril, triu, diag:


for i = 1:5
tic
density = 10;
[Edges]=generate_network(N(i),density,172090);
d=0.85;
B = sparse(Edges(2,:),Edges(1,:),1);  % macierze A, B i I musz� by� przechowywane w formacie sparse (rzadkim)
A = sparse(diag(1./sum(B)));
I = speye(N(i));
M = I-d*B*A;
b(1:N(i),1)=(1-d)/N(i);
L=tril(M,-1);
U=triu(M,1);
D=diag(diag(M));
r=ones(N(i),1);
iterations=1;
while 1
    r = -inv(D)*(L+U)*r+inv(D)*b;
    norm_value(iterations)=norm(M*r-b);
    if(norm_value(iterations) < 10^-14)
        break;
    end
    iterations=iterations+1;
end
figure();
semilogy(norm_value);
saveas (gcf,strcat('zadE_172090_residuum',int2str(i),'.png'));
iter(i)=iterations;
czas_Jacobi(i) = toc;
end
figure();
plot(N, czas_Jacobi)
saveas (gcf,strcat('zadE_172090_time.png'));
figure();
plot(N,iter)
saveas (gcf,strcat('zadE_172090_iters.png'));
%------------------

% Zadanie F
%------------------
clc
clear all
close all
N = [500, 1000, 3000, 6000, 12000];
% przyk�ad dzia�ania funkcji tril, triu, diag:


for i = 1:5
tic
density = 10;
[Edges]=generate_network(N(i),density,172090);
d=0.85;
B = sparse(Edges(2,:),Edges(1,:),1);  % macierze A, B i I musz� by� przechowywane w formacie sparse (rzadkim)
A = sparse(diag(1./sum(B)));
I = speye(N(i));
M = I-d*B*A;
b(1:N(i),1)=(1-d)/N(i);
L=tril(M,-1);
U=triu(M,1);
D=diag(diag(M));
r=ones(N(i),1);
iterations=1;
while 1
    r = -(D+L)\(U*r)+(D+L)\b;
    norm_value(iterations)=norm(M*r-b);
    if(norm_value(iterations) < 10^-14)
        break;
    end
    iterations=iterations+1;
end
figure();
semilogy(norm_value);
saveas (gcf,strcat('zadF_172090_residuum',int2str(i),'.png'));
iter(i)=iterations;
czas_Gauss(i) = toc;
end
figure();
plot(N, czas_Gauss)
saveas (gcf,strcat('zadF_172090_time.png'));
figure();
plot(N,iter)
saveas (gcf,strcat('zadF_172090_iters.png'));
%------------------


% Zadanie G
%------------------
clc
clear all
close all
N = 20000;
% przyk�ad dzia�ania funkcji tril, triu, diag:
load('Dane_Filtr_Dielektryczny_lab3_MN.mat');
L=tril(M,-1);
U=triu(M,1);
D=diag(diag(M));
wyniki = fopen('wyniki.txt','w');

%METODA BEZPO�REDNIA
tic
r=(M\b)';
czas_Gauss = toc;
fprintf(wyniki,'Metoda Bezpo�rednia:\n Czas: %f \n',czas_Gauss);

%METODA JACOBIEGO
tic
r=ones(N,1);
iterations=1;
while norm(M*r-b) > 10^-14
    r = -inv(D)*(L+U)*r+inv(D)*b;
    norm_value(iterations)=norm(M*r-b);
    iterations=iterations+1;
end
figure();
semilogy(norm_value);
saveas (gcf,strcat('zadG_172090_Jacobi_residuum.png'));
czas_Jacobi = toc;
fprintf(wyniki,'Metoda Jacobiego:\n Czas: %f \n Iteracje: %f \n',czas_Jacobi,iterations);

%METODA GAUSSA-SEIDLA
tic
r=ones(N,1);
iterations=1;
while norm(M*r-b) > 10^-14
    r = (-(D+L)\(U*r)+(D+L)\b);
    residu(iterations)=norm(M*r-b);
    iterations=iterations+1;
end
figure();
semilogy(residu);
saveas (gcf,strcat('zadG_172090_Gauss-Seidle__residuum.png'));
czas_Gauss = toc;
fprintf(wyniki,'Metoda Gaussa:\n Czas: %f \n Iteracje: %f \n',czas_Gauss,iterations);
fclose(fileID);
%------------------



