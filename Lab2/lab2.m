N=7;
d=0.85;

Edges=[1,1,2,2,2,3,3,3,4,4,5,5,6,6,7;4,6,3,4,5,5,6,7,5,6,4,6,4,7,6];
I=eye(N);
B=full(sparse(Edges(2,:),Edges(1,:),1));
A=sparse(diag(1./sum(B)));
b=[];
b=((1-d)/N)*ones(N,1);
r=[];
M=I-d*B*A;
r=M\b;
r=r/norm(r);
bar(r);

