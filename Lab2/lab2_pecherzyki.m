a=1;
r_max=0.1;
n_max=10000;
n=0;
loops=0;
x=zeros(1,n_max);
y=zeros(1,n_max);
r=zeros(1,n_max);
field=zeros(1,n_max);
summed_field=zeros(1,n_max);
loops_needed=zeros(1,n_max);

fill([0,0,a,a,0],[0,a,a,0,0],'k');
hold on;

while(n<n_max)
    loops=loops+1;
    x_rand=rem(rand(),a);
    y_rand=rem(rand(),a);
    r_rand=rem(rand(),r_max);
    if(x_rand+r_rand < a && y_rand+r_rand < a && x_rand-r_rand >0 && y_rand-r_rand>0)
        if(sqrt((x - x_rand).^2 + (y - y_rand).^2) > (r + r_rand))
        axis equal;
        plot_circle(x_rand,y_rand,r_rand);
        hold on;
        n=n+1;
        x(1,n)=x_rand;
        y(1,n)=y_rand;
        r(1,n)=r_rand;
        field(1,n)=pi*r_rand^2;
        summed_field(1,n)=sum(field);
        loops_needed(1,n)=loops;
        loops=0;
        fprintf(1, ' %s%5d%s%.4g\r ', 'n =',  n, ' S = ', sum(field));
        pause(0.01);
        end
    end
end

figure();
semilogx(summed_field);

function plot_circle(X, Y, R)
theta = linspace(0,2*pi);
x = R*cos(theta) + X;
y = R*sin(theta) + Y;
plot(x,y)
end



