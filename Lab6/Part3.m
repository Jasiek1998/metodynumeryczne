clc
clear all
close all

load trajektoria2

grid on;
axis equal;
subplot(2,1,1);
plot3(x,y,z,'o');
title('Position of drone based only on localization');
xlabel('x position');
ylabel('y position');
zlabel('z position');


subplot(2,1,2);
N = 50;
[ xa ] = aprox_tryg(N,n,x);  % aproksymacja wsp. 'x'
[ ya ] = aprox_tryg(N,n,y);
[ za ] = aprox_tryg(N,n,z);

plot3(xa,ya,za,'lineWidth',4);
title('Position of drone based on aproximation of localization');
xlabel('x position');
ylabel('y position');
zlabel('z position');
saveas(gcf,'./Plots/172090_Kostrzebski_zad6.png');



err = [];
for i=1:71 
   [ xa ] = aprox_tryg(i,n,x);  % aproksymacja wsp. 'x'
   [ ya ] = aprox_tryg(i,n,y);
   [ za ] = aprox_tryg(i,n,z);
   errx=sqrt((sum((x-xa').^2)))/length(x);
   erry=sqrt((sum((y-ya').^2)))/length(y);
   errz=sqrt((sum((z-za').^2)))/length(z);
   err(i) = errx+erry+errz;
end

figure();
semilogy(err);
title('Error plot trigonometric approximation');
xlabel('Number of trigonometric functions');
ylabel('Error value');
saveas(gcf,'./Plots/172090_Kostrzebski_zad6_b.png');
