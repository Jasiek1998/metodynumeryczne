load trajektoria1

grid on;
axis equal;
subplot(2,1,1);
plot3(x,y,z,'o');
title('Position of drone based only on localization');
xlabel('x position');
ylabel('y position');
zlabel('z position');


subplot(2,1,2);
N = 50;
[ wsp_wielomianu, xa ] = aproksymacjaWiel(n,x,N);  % aproksymacja wsp. 'x'.
[ wsp_wielomianu, ya ] = aproksymacjaWiel(n,y,N);
[ wsp_wielomianu, za ] = aproksymacjaWiel(n,z,N);

plot3(xa,ya,za,'lineWidth',4);
title('Position of drone based on aproximation of localization');
xlabel('x position');
ylabel('y position');
zlabel('z position');
saveas(gcf,'./Plots/172090_Kostrzebski_zad4.png');