function [value] = give_value(x)
u=10;
o=3;
nominator = exp(1).^-((x-u).^2/(2*o.^2));
denominator = o*sqrt(2*pi);

value = nominator/denominator;
end

