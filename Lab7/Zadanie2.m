clc
clear all
close all

load dane_jezioro   % dane XX, YY, FF sa potrzebne jedynie do wizualizacji problemu. 
surf(XX,YY,FF)
shading interp
axis equal

N=1e5;
N1=0;
V=100*100*50;
xvec = 100.*rand(1,N);
yvec = 100.*rand(1,N);
zvec = -50.*rand(1,N);

for i=1:N
    if glebokosc(xvec(i),yvec(i))<zvec(i)
        N1=N1+1;
    end
end
wyniki = fopen('wynik_zadanie2.txt','w');
volume = V*N1/N;
fprintf(wyniki,'G��boko�� zbiornika: %d m^3\n',volume);
fclose(wyniki);