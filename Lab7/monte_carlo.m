function [sum] = monte_carlo(x)
sum=0;
start=0;
ending=5;
minimum = give_value(start);
maximum =  give_value(ending);
height = maximum-minimum;
width = ending-start;
N1=0;
S=width*height;
xvec = (width).*rand(1,x)+start;
yvec = (height).*rand(1,x)+minimum;

for i=1:x
    if give_value(xvec(i))>yvec(i)
        N1=N1+1;
    end
end

sum = S*N1/x;


end

